/* AUTO-GENERATED FILE.  DO NOT MODIFY.
 *
 * This class was automatically generated by the
 * gradle plugin from the resource data it found. It
 * should not be modified by hand.
 */
package com.google.android.gms;

public final class R {
    public static final class attr {
        public static final int ambientEnabled = 0x7f030000;
        public static final int appTheme = 0x7f030001;
        public static final int buttonSize = 0x7f030002;
        public static final int buyButtonAppearance = 0x7f030003;
        public static final int buyButtonHeight = 0x7f030004;
        public static final int buyButtonText = 0x7f030005;
        public static final int buyButtonWidth = 0x7f030006;
        public static final int cameraBearing = 0x7f030007;
        public static final int cameraMaxZoomPreference = 0x7f030008;
        public static final int cameraMinZoomPreference = 0x7f030009;
        public static final int cameraTargetLat = 0x7f03000a;
        public static final int cameraTargetLng = 0x7f03000b;
        public static final int cameraTilt = 0x7f03000c;
        public static final int cameraZoom = 0x7f03000d;
        public static final int circleCrop = 0x7f03000e;
        public static final int colorScheme = 0x7f03000f;
        public static final int environment = 0x7f030010;
        public static final int fragmentMode = 0x7f030011;
        public static final int fragmentStyle = 0x7f030012;
        public static final int imageAspectRatio = 0x7f030013;
        public static final int imageAspectRatioAdjust = 0x7f030014;
        public static final int latLngBoundsNorthEastLatitude = 0x7f030015;
        public static final int latLngBoundsNorthEastLongitude = 0x7f030016;
        public static final int latLngBoundsSouthWestLatitude = 0x7f030017;
        public static final int latLngBoundsSouthWestLongitude = 0x7f030018;
        public static final int liteMode = 0x7f030019;
        public static final int mapType = 0x7f03001a;
        public static final int maskedWalletDetailsBackground = 0x7f03001b;
        public static final int maskedWalletDetailsButtonBackground = 0x7f03001c;
        public static final int maskedWalletDetailsButtonTextAppearance = 0x7f03001d;
        public static final int maskedWalletDetailsHeaderTextAppearance = 0x7f03001e;
        public static final int maskedWalletDetailsLogoImageType = 0x7f03001f;
        public static final int maskedWalletDetailsLogoTextColor = 0x7f030020;
        public static final int maskedWalletDetailsTextAppearance = 0x7f030021;
        public static final int scopeUris = 0x7f030022;
        public static final int toolbarTextColorStyle = 0x7f030023;
        public static final int uiCompass = 0x7f030024;
        public static final int uiMapToolbar = 0x7f030025;
        public static final int uiRotateGestures = 0x7f030026;
        public static final int uiScrollGestures = 0x7f030027;
        public static final int uiTiltGestures = 0x7f030028;
        public static final int uiZoomControls = 0x7f030029;
        public static final int uiZoomGestures = 0x7f03002a;
        public static final int useViewLifecycle = 0x7f03002b;
        public static final int windowTransitionStyle = 0x7f03002c;
        public static final int zOrderOnTop = 0x7f03002d;
    }
    public static final class color {
        public static final int common_google_signin_btn_text_dark = 0x7f050000;
        public static final int common_google_signin_btn_text_dark_default = 0x7f050001;
        public static final int common_google_signin_btn_text_dark_disabled = 0x7f050002;
        public static final int common_google_signin_btn_text_dark_focused = 0x7f050003;
        public static final int common_google_signin_btn_text_dark_pressed = 0x7f050004;
        public static final int common_google_signin_btn_text_light = 0x7f050005;
        public static final int common_google_signin_btn_text_light_default = 0x7f050006;
        public static final int common_google_signin_btn_text_light_disabled = 0x7f050007;
        public static final int common_google_signin_btn_text_light_focused = 0x7f050008;
        public static final int common_google_signin_btn_text_light_pressed = 0x7f050009;
        public static final int common_google_signin_btn_tint = 0x7f05000a;
        public static final int wallet_bright_foreground_disabled_holo_light = 0x7f050016;
        public static final int wallet_bright_foreground_holo_dark = 0x7f050017;
        public static final int wallet_bright_foreground_holo_light = 0x7f050018;
        public static final int wallet_dim_foreground_disabled_holo_dark = 0x7f050019;
        public static final int wallet_dim_foreground_holo_dark = 0x7f05001a;
        public static final int wallet_highlighted_text_holo_dark = 0x7f05001b;
        public static final int wallet_highlighted_text_holo_light = 0x7f05001c;
        public static final int wallet_hint_foreground_holo_dark = 0x7f05001d;
        public static final int wallet_hint_foreground_holo_light = 0x7f05001e;
        public static final int wallet_holo_blue_light = 0x7f05001f;
        public static final int wallet_link_text_light = 0x7f050020;
        public static final int wallet_primary_text_holo_light = 0x7f050021;
        public static final int wallet_secondary_text_holo_dark = 0x7f050022;
    }
    public static final class drawable {
        public static final int common_full_open_on_phone = 0x7f060061;
        public static final int common_google_signin_btn_icon_dark = 0x7f060062;
        public static final int common_google_signin_btn_icon_dark_focused = 0x7f060063;
        public static final int common_google_signin_btn_icon_dark_normal = 0x7f060064;
        public static final int common_google_signin_btn_icon_dark_normal_background = 0x7f060065;
        public static final int common_google_signin_btn_icon_disabled = 0x7f060066;
        public static final int common_google_signin_btn_icon_light = 0x7f060067;
        public static final int common_google_signin_btn_icon_light_focused = 0x7f060068;
        public static final int common_google_signin_btn_icon_light_normal = 0x7f060069;
        public static final int common_google_signin_btn_icon_light_normal_background = 0x7f06006a;
        public static final int common_google_signin_btn_text_dark = 0x7f06006b;
        public static final int common_google_signin_btn_text_dark_focused = 0x7f06006c;
        public static final int common_google_signin_btn_text_dark_normal = 0x7f06006d;
        public static final int common_google_signin_btn_text_dark_normal_background = 0x7f06006e;
        public static final int common_google_signin_btn_text_disabled = 0x7f06006f;
        public static final int common_google_signin_btn_text_light = 0x7f060070;
        public static final int common_google_signin_btn_text_light_focused = 0x7f060071;
        public static final int common_google_signin_btn_text_light_normal = 0x7f060072;
        public static final int common_google_signin_btn_text_light_normal_background = 0x7f060073;
        public static final int googleg_disabled_color_18 = 0x7f0600ad;
        public static final int googleg_standard_color_18 = 0x7f0600ae;
    }
    public static final class id {
        public static final int adjust_height = 0x7f070000;
        public static final int adjust_width = 0x7f070001;
        public static final int android_pay = 0x7f070002;
        public static final int android_pay_dark = 0x7f070003;
        public static final int android_pay_light = 0x7f070004;
        public static final int android_pay_light_with_border = 0x7f070005;
        public static final int auto = 0x7f070006;
        public static final int book_now = 0x7f070007;
        public static final int buyButton = 0x7f070013;
        public static final int buy_now = 0x7f070014;
        public static final int buy_with = 0x7f070015;
        public static final int buy_with_google = 0x7f070016;
        public static final int classic = 0x7f070017;
        public static final int dark = 0x7f070030;
        public static final int donate_with = 0x7f070031;
        public static final int donate_with_google = 0x7f070032;
        public static final int google_wallet_classic = 0x7f070034;
        public static final int google_wallet_grayscale = 0x7f070035;
        public static final int google_wallet_monochrome = 0x7f070036;
        public static final int grayscale = 0x7f070037;
        public static final int holo_dark = 0x7f070039;
        public static final int holo_light = 0x7f07003a;
        public static final int hybrid = 0x7f07003b;
        public static final int icon_only = 0x7f07003c;
        public static final int light = 0x7f070049;
        public static final int logo_only = 0x7f07004d;
        public static final int match_parent = 0x7f07004e;
        public static final int monochrome = 0x7f070053;
        public static final int none = 0x7f070055;
        public static final int normal = 0x7f070056;
        public static final int production = 0x7f070061;
        public static final int sandbox = 0x7f070062;
        public static final int satellite = 0x7f070063;
        public static final int selectionDetails = 0x7f070068;
        public static final int slide = 0x7f070069;
        public static final int standard = 0x7f07006a;
        public static final int strict_sandbox = 0x7f07006b;
        public static final int terrain = 0x7f07006e;
        public static final int test = 0x7f07006f;
        public static final int wide = 0x7f070073;
        public static final int wrap_content = 0x7f070074;
    }
    public static final class integer {
        public static final int google_play_services_version = 0x7f080000;
    }
    public static final class string {
        public static final int common_google_play_services_enable_button = 0x7f0b07bc;
        public static final int common_google_play_services_enable_text = 0x7f0b07bd;
        public static final int common_google_play_services_enable_title = 0x7f0b07be;
        public static final int common_google_play_services_install_button = 0x7f0b07bf;
        public static final int common_google_play_services_install_text = 0x7f0b07c0;
        public static final int common_google_play_services_install_title = 0x7f0b07c1;
        public static final int common_google_play_services_notification_ticker = 0x7f0b07c2;
        public static final int common_google_play_services_unknown_issue = 0x7f0b07c3;
        public static final int common_google_play_services_unsupported_text = 0x7f0b07c4;
        public static final int common_google_play_services_update_button = 0x7f0b07c5;
        public static final int common_google_play_services_update_text = 0x7f0b07c6;
        public static final int common_google_play_services_update_title = 0x7f0b07c7;
        public static final int common_google_play_services_updating_text = 0x7f0b07c8;
        public static final int common_google_play_services_wear_update_text = 0x7f0b07c9;
        public static final int common_open_on_phone = 0x7f0b07ca;
        public static final int common_signin_button_text = 0x7f0b07cb;
        public static final int common_signin_button_text_long = 0x7f0b07cc;
        public static final int wallet_buy_button_place_holder = 0x7f0b0839;
    }
    public static final class style {
        public static final int WalletFragmentDefaultButtonTextAppearance = 0x7f0c0014;
        public static final int WalletFragmentDefaultDetailsHeaderTextAppearance = 0x7f0c0015;
        public static final int WalletFragmentDefaultDetailsTextAppearance = 0x7f0c0016;
        public static final int WalletFragmentDefaultStyle = 0x7f0c0017;
    }
    public static final class styleable {
        public static final int[] CustomWalletTheme = { 0x7f030023, 0x7f03002c };
        public static final int CustomWalletTheme_toolbarTextColorStyle = 0;
        public static final int CustomWalletTheme_windowTransitionStyle = 1;
        public static final int[] LoadingImageView = { 0x7f03000e, 0x7f030013, 0x7f030014 };
        public static final int LoadingImageView_circleCrop = 0;
        public static final int LoadingImageView_imageAspectRatio = 1;
        public static final int LoadingImageView_imageAspectRatioAdjust = 2;
        public static final int[] MapAttrs = { 0x7f030000, 0x7f030007, 0x7f030008, 0x7f030009, 0x7f03000a, 0x7f03000b, 0x7f03000c, 0x7f03000d, 0x7f030015, 0x7f030016, 0x7f030017, 0x7f030018, 0x7f030019, 0x7f03001a, 0x7f030024, 0x7f030025, 0x7f030026, 0x7f030027, 0x7f030028, 0x7f030029, 0x7f03002a, 0x7f03002b, 0x7f03002d };
        public static final int MapAttrs_ambientEnabled = 0;
        public static final int MapAttrs_cameraBearing = 1;
        public static final int MapAttrs_cameraMaxZoomPreference = 2;
        public static final int MapAttrs_cameraMinZoomPreference = 3;
        public static final int MapAttrs_cameraTargetLat = 4;
        public static final int MapAttrs_cameraTargetLng = 5;
        public static final int MapAttrs_cameraTilt = 6;
        public static final int MapAttrs_cameraZoom = 7;
        public static final int MapAttrs_latLngBoundsNorthEastLatitude = 8;
        public static final int MapAttrs_latLngBoundsNorthEastLongitude = 9;
        public static final int MapAttrs_latLngBoundsSouthWestLatitude = 10;
        public static final int MapAttrs_latLngBoundsSouthWestLongitude = 11;
        public static final int MapAttrs_liteMode = 12;
        public static final int MapAttrs_mapType = 13;
        public static final int MapAttrs_uiCompass = 14;
        public static final int MapAttrs_uiMapToolbar = 15;
        public static final int MapAttrs_uiRotateGestures = 16;
        public static final int MapAttrs_uiScrollGestures = 17;
        public static final int MapAttrs_uiTiltGestures = 18;
        public static final int MapAttrs_uiZoomControls = 19;
        public static final int MapAttrs_uiZoomGestures = 20;
        public static final int MapAttrs_useViewLifecycle = 21;
        public static final int MapAttrs_zOrderOnTop = 22;
        public static final int[] SignInButton = { 0x7f030002, 0x7f03000f, 0x7f030022 };
        public static final int SignInButton_buttonSize = 0;
        public static final int SignInButton_colorScheme = 1;
        public static final int SignInButton_scopeUris = 2;
        public static final int[] WalletFragmentOptions = { 0x7f030001, 0x7f030010, 0x7f030011, 0x7f030012 };
        public static final int WalletFragmentOptions_appTheme = 0;
        public static final int WalletFragmentOptions_environment = 1;
        public static final int WalletFragmentOptions_fragmentMode = 2;
        public static final int WalletFragmentOptions_fragmentStyle = 3;
        public static final int[] WalletFragmentStyle = { 0x7f030003, 0x7f030004, 0x7f030005, 0x7f030006, 0x7f03001b, 0x7f03001c, 0x7f03001d, 0x7f03001e, 0x7f03001f, 0x7f030020, 0x7f030021 };
        public static final int WalletFragmentStyle_buyButtonAppearance = 0;
        public static final int WalletFragmentStyle_buyButtonHeight = 1;
        public static final int WalletFragmentStyle_buyButtonText = 2;
        public static final int WalletFragmentStyle_buyButtonWidth = 3;
        public static final int WalletFragmentStyle_maskedWalletDetailsBackground = 4;
        public static final int WalletFragmentStyle_maskedWalletDetailsButtonBackground = 5;
        public static final int WalletFragmentStyle_maskedWalletDetailsButtonTextAppearance = 6;
        public static final int WalletFragmentStyle_maskedWalletDetailsHeaderTextAppearance = 7;
        public static final int WalletFragmentStyle_maskedWalletDetailsLogoImageType = 8;
        public static final int WalletFragmentStyle_maskedWalletDetailsLogoTextColor = 9;
        public static final int WalletFragmentStyle_maskedWalletDetailsTextAppearance = 10;
    }
}
